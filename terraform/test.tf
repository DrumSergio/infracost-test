terraform {
  required_version = ">= 1.1"

  backend "gcs" {
    bucket = "hha-switchh-integration-37031-de-clara-net"
    prefix = "terraform/gke-cluster"
  }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.9.0"
    }
  }
}


provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_default_region
  zone    = var.gcp_default_zone
}

#====================
# Project variables 
#====================
variable "gcp_project_id" {
  default = "claranet-playground"
}

variable "gcp_default_region" {
  default = "us-east4"
}

variable "gcp_default_zone" {
  default = "us-east4-a"
}

locals {
  vpc = {
    node_subnet_ip_range    = "10.162.4.0/22"
    pod_subnet_ip_range     = "10.43.0.0/17"
    service_subnet_ip_range = "10.143.2.0/23"
    pod_subnet_name         = "gke-pod-alias-ips"
  }

  node_nat_ports_per_vm   = 1024
  pod_nat_ports_per_vm    = 64

  cluster = {
      name                   = "whatever"
      min_master_version     = "1.11.6-gke.2"
      master_ipv4_cidr_block = "172.16.0.32/28"
      daily_maintenance      = "03:00"
      tags                   = ["gke"]
      node_disk_size_gb      = 100
      node_disk_type         = "pd-standard"
      node_image_type        = "COS"
  }
  stateful = {
    node_version       = "1.20.10-gke.2100"
    initial_node_count = 8
    machine_type       = "n1-highmem-4"
  }
}


#================================
# Virtual Private Clouds (VPCs)
#================================

resource "google_compute_network" "gke-vpc" {
  name                    = "gke-cluster"
  description             = "GKE Cluster Network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "gke-vpc-subnet" {
  name                     = "gke-worker-nodes"
  description                = "Subnet for worker nodes from GKE"
  ip_cidr_range            = local.vpc.node_subnet_ip_range
  network                  = google_compute_network.gke-vpc.self_link
  private_ip_google_access = true
  secondary_ip_range {
    range_name    = local.vpc.pod_subnet_name
    ip_cidr_range = local.vpc.pod_subnet_ip_range
  }
  secondary_ip_range {
    range_name    = "gke-services"
    ip_cidr_range = local.vpc.service_subnet_ip_range
  }
}

#=================
# CLOUD NAT NODE
#=================

resource "google_compute_router" "gke-node-nat" {
  name    = "gke-node-nat"
  network = google_compute_network.gke-vpc.self_link
}

resource "google_compute_address" "gke-node-nat" {
  name   = "gke-node-nat-external-address"
}

resource "google_compute_address" "gke-node-nat_1" {
  name   = "gke-node-nat-external-address-1"
}

resource "google_compute_router_nat" "gke-node-nat" {
  name                               = "gke-node-nat"
  router                             = google_compute_router.gke-node-nat.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.gke-node-nat.self_link, google_compute_address.gke-node-nat_1.self_link]
  min_ports_per_vm                   = local.node_nat_ports_per_vm
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                    = google_compute_subnetwork.gke-vpc-subnet.self_link
    source_ip_ranges_to_nat = ["PRIMARY_IP_RANGE"]
  }
  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

#================
# CLOUD NAT POD
#================

resource "google_compute_router" "gke-pod-nat" {
  name    = "gke-pod-nat"
  network = google_compute_network.gke-vpc.self_link
}

resource "google_compute_address" "gke-pod-nat" {
  name   = "gke-pod-nat-external-address"
}

resource "google_compute_router_nat" "gke-pod-nat" {
  name                               = "gke-pod-nat"
  router                             = google_compute_router.gke-pod-nat.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.gke-pod-nat.self_link]
  min_ports_per_vm                   = local.pod_nat_ports_per_vm
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  subnetwork {
    name                     = google_compute_subnetwork.gke-vpc-subnet.self_link
    source_ip_ranges_to_nat  = ["LIST_OF_SECONDARY_IP_RANGES"]
    secondary_ip_range_names = [local.vpc.pod_subnet_name]
  }
  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

#==============
# Firewalling
#==============
resource "google_compute_firewall" "mgmt" {
  name        = "${google_compute_network.gke-vpc.name}-allow-mgmt-access"
  network     = google_compute_network.gke-vpc.name
  description = "Restrict managment access to source prefixes"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443"]
  }

  source_ranges = local.fw_mgmt_sources
}

resource "google_compute_firewall" "monitoring" {
  name        = "${google_compute_network.gke-vpc.name}-allow-monitoring"
  network     = google_compute_network.gke-vpc.name
  description = "Permit access for on-prem monitoring"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443", "5666"]
  }

  source_ranges = local.fw_mon_sources
}

#==============
# GKE Cluster
#==============
resource "google_container_cluster" "gke-cluster" {
  name               = local.cluster.name
  min_master_version = local.cluster.min_master_version
  location           = var.gcp_default_region
  enable_shielded_nodes       = false

  # this is needed even if the actual count is defined in the pools
  initial_node_count       = "1"
  remove_default_node_pool = "true"
  enable_legacy_abac       = "false"
  network                  = google_compute_network.gke-vpc.name
  subnetwork               = google_compute_subnetwork.gke-vpc-subnet.name

  # This attribute requires the beta version
  private_cluster_config {
    enable_private_nodes   = true
    master_ipv4_cidr_block = local.cluster.master_ipv4_cidr_block
    enable_private_endpoint = false
  }


  # Set at least empty to activate aliasIP needed by VPC native cluster
  ip_allocation_policy {
    cluster_secondary_range_name  = google_compute_subnetwork.gke-vpc-subnet.secondary_ip_range[0].range_name
    services_secondary_range_name = google_compute_subnetwork.gke-vpc-subnet.secondary_ip_range[1].range_name
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = local.cluster.daily_maintenance
    }
  }

  # disable basic auth
  master_auth {
    client_certificate_config {
      issue_client_certificate = true
    } 
  }

  addons_config {
    http_load_balancing {
      disabled = false
    }
    network_policy_config {
      disabled = false
    }
  }

  network_policy {
    enabled  = true
    provider = "CALICO"
  }
}

#=========================
# GKE NODE POOL Stateful
#=========================

resource "google_container_node_pool" "stateful-1-23-12-gke-1600" {
  name               = "stateful-1-23-12-gke-1600"
  cluster            = google_container_cluster.gke-cluster.name
  version            = "1.23.12-gke.1600"
  initial_node_count = 40
  location           = "us-east4"

  management {
    auto_repair  = true 
    auto_upgrade = false
  }

  node_config {
    preemtible   = true
    machine_type = local.stateful.machine_type
    disk_size_gb = local.cluster.node_disk_size_gb
    disk_type    = local.cluster.node_disk_type
    image_type   = "COS_CONTAINERD"
    oauth_scopes = local.cluster.node_permissions

    labels = {
      node_pool = "stateful"
    }

  }
}

#=========================
# Global Reserved IPs
#=========================

resource "google_compute_address" "gke-platform-services" {
  name        = "gke-platform-services"
  description = "Address for Claranet Platform Services"
}

